﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Get all employees data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Get employee data by id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,                
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
                //Roles = employee.Roles.Select(x => new RoleItemResponse()
                //{
                //    Name = x.Name,
                //    Description = x.Description
                //}).ToList(),

            };

            return employeeModel;
        }

        [HttpPost]
        public async Task<StatusCodeResult> CreateEmployee(Employee obj)
        {
            var res = await _employeeRepository.CreateAsync(obj);

            if (res != "Ok")
                return NotFound();
            else
                return Ok();
        }

        [HttpPut]
        public async Task<StatusCodeResult> UpdateEmployee(Employee obj)
        {
            var res = await _employeeRepository.UpdateAsync(obj);

            if (res != "Ok")
                return NotFound();
            else
                return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<StatusCodeResult> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            
            if (employee == null)
                return NotFound();

            var res = await _employeeRepository.DeleteAsync(employee /*id*/);

            if (res != "Ok")
                return NotFound();
            else
                return Ok();
        }
    }


}