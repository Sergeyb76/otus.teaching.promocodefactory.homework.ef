﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public interface IDbInit
    {
        public void InitiDb();
    }
}