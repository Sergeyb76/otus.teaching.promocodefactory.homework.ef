﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInit
        : IDbInit
    {
        private readonly DatabaseContext _dbContext;

        public EfDbInit(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public void InitiDb()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.EnsureCreated();

            _dbContext.AddRange(FakeDataFactory.Customers);
            _dbContext.SaveChanges();

            _dbContext.AddRange(FakeDataFactory.Preferences);
            _dbContext.SaveChanges();

            // ...

        }
    }
}