﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
            // ...
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
            // ...
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // base.OnConfiguring(optionsBuilder);            
            //optionsBuilder.UseSqlite(@"Data Source=d:\db\PromoCodedb.db");
        }

        //override void OnModelCreating(ModelBuilder modelBuilder)
        //{
            //modelBuilder.Entity<MainPromoCodePartnerPreference>()
            //    .HasKey(bc => new { bc.PartnerId, bc.PreferenceId });

            //modelBuilder.Entity<MainPromoCodePartnerPreference>()
            //    .HasOne(bc => bc.Partner)
            //    .WithMany(b => b.MainPromoCodePreferences)
            //    .HasForeignKey(bc => bc.PartnerId);

            //modelBuilder.Entity<MainPromoCodePartnerPreference>()
            //    .HasOne(bc => bc.Preference)
            //    .WithMany()
            //    .HasForeignKey(bc => bc.PreferenceId);

            //modelBuilder.Entity<MainPromoCodePartnerPreference>().ToTable("MainPromoCodePartnerPreferences");
            //modelBuilder.Entity<PartnerPromoCodeLimit>().ToTable("PartnerPromoCodeLimits");
        //}
    }
}
