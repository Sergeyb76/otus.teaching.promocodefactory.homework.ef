﻿//using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
//using Otus.Teaching.PromoCodeFactory.Core.Domain;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private DatabaseContext _dbContext;

        public EfRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<string> CreateAsync(T obj)
        {
            await _dbContext.Set<T>().AddAsync(obj);
            await _dbContext.SaveChangesAsync();

            return await Task.FromResult("Ok");
        }

        public async Task<string> UpdateAsync(T obj)
        {
            await _dbContext.SaveChangesAsync();

            return await Task.FromResult("Ok");
        }

        //public async Task<string> DeleteAsync(T element)
        public async Task<string> DeleteAsync(T obj /*(Guid id*/)
        {
            _dbContext.Set<T>().Remove(obj);
            await _dbContext.SaveChangesAsync();

            return await Task.FromResult("Ok");
        }
    }
}
